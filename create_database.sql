create table APP.MATEUSZKEDZIERSKI01_NAPOJE_TB
(
    ID_NAPOJU                  INTEGER not null
        constraint "mateusz.kedzierski01_napoje_tb_pk"
            primary key,
    NAZWA                      VARCHAR(20),
    TYP                        VARCHAR(15),
    SMAK                       VARCHAR(10),
    KOLOR                      VARCHAR(10),
    CENA_PLN                   INTEGER,
    CZY_TYLKO_DLA_PELNOLETNICH BOOLEAN
);

