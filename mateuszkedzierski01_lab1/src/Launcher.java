public class Launcher {

    /**
     * Ta klasa jest potrzebna do uruchamiania, poniewaz w JavaFX klasa rozszerzajaca Application nie
     * moze zostac uruchomiona z pliku JAR.
     *
     * @param args argumenty uruchomienia programu
     */
    public static void main(String[] args) {
        App.main(args);
    }

}