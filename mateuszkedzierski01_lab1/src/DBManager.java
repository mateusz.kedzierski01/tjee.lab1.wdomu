import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DBManager {
    public static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String PROTOCOL = "jdbc:derby:";
    public static final String DB_PATH = "mateuszkedzierski01_db";
    public static final String JDBC_URL = PROTOCOL + DB_PATH;
    private static Connection conn;


    public static void connect() throws SQLException {
        conn = DriverManager.getConnection(JDBC_URL);
    }

    public static void disconnect() throws SQLException {
        conn.close();
        conn = null;
    }

    public static ObservableList<Napoj> getData() throws SQLException {
        Statement stat = conn.createStatement();
        String query = "SELECT * FROM MATEUSZKEDZIERSKI01_NAPOJE_TB";
        ResultSet rs = stat.executeQuery(query);

        List<Napoj> list = new ArrayList<>();
        while (rs.next()) {
            Object[] args = {rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getBoolean(7)};
            Boolean[] val = Controller.valNapoj((String) args[1], (String) args[2], (String) args[3], (String) args[4], args[5].toString());
            List<Boolean> valList = Arrays.asList(val);
            if (valList.contains(false)) {
                Controller.log("Niepoprawne dane w bazie! ID: " + args[0] + " nazwa: " + args[1]);
                continue;
            }
            Napoj n = new Napoj((int) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4], (int) args[5], (boolean) args[6]);
            list.add(n);
        }
        stat.close();
        return FXCollections.observableList(list);
    }

    public static void insertData(Napoj n) throws SQLException {
        Statement stat = conn.createStatement();

        String polecenie = "INSERT INTO MATEUSZKEDZIERSKI01_NAPOJE_TB(ID_NAPOJU, NAZWA, TYP, SMAK, KOLOR, CENA_PLN, CZY_TYLKO_DLA_PELNOLETNICH) VALUES(" +
                n.getId() + ", '" +
                n.getNazwa() + "', '" +
                n.getTyp() + "', '" +
                n.getSmak() + "', '" +
                n.getKolor() + "', " +
                n.getCena() + ", " +
                n.is18plus() + ")";
        stat.executeUpdate(polecenie);
        stat.close();
    }

    public static void updateData(Napoj n) throws SQLException {
        Statement stat = conn.createStatement();
        //System.out.println("Aktualizuję rekord o ID: " + n.getId());
        String polecenie = "UPDATE MATEUSZKEDZIERSKI01_NAPOJE_TB SET " +
                "NAZWA = '" + n.getNazwa() +
                "', TYP = '" + n.getTyp() +
                "', SMAK = '" + n.getSmak() +
                "' , KOLOR = '" + n.getKolor() +
                "', CENA_PLN = " + n.getCena() +
                ", CZY_TYLKO_DLA_PELNOLETNICH = " + n.is18plus() +
                " WHERE ID_NAPOJU=" + n.getId();
        //System.out.println("Zaktualizowano " +
        stat.executeUpdate(polecenie);
        // + " rekordów");
        stat.close();
    }

    public static int delete(Napoj n) throws SQLException {
        int id = n.getId();
        Statement stat = conn.createStatement();
        String polecenie = "DELETE FROM MATEUSZKEDZIERSKI01_NAPOJE_TB WHERE ID_NAPOJU=" + id;
        return stat.executeUpdate(polecenie);
    }

    public static int getFirstFreeID() {
        String QUERY = "SELECT * FROM MATEUSZKEDZIERSKI01_NAPOJE_TB ORDER BY ID_NAPOJU ASC";
        Statement stat;
        int id = 0;
        try {
            stat = conn.createStatement();

            try {
                ResultSet rs = stat.executeQuery(QUERY);
                int i = 0;
                while (rs.next()) {
                    if (rs.getInt("ID_NAPOJU") != i) break;
                    i++;
                }
                id = i;
                //System.out.println("Pierwsze wolne ID: " + id);
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    public static boolean isConnected() {
        return conn != null;
    }

}

