import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Glowna klasa aplikacji JavaFX, sluzy do uruchomienia aplikacji i wykonania czynnosci poczatkowych po uruchomieniu.
 */
public class App extends Application {
    private static Stage stage;
    private static TextArea cons;
    private static FXMLLoader fxmlLoader;

    /**
     * Glowna metoda programu. Uruchamia aplikację główną JavaFX.
     *
     * @param args argumenty uruchomienia
     */
    public static void main(String[] args) {
        launch();
    }

    /**
     * Zwraca loader, ktory laduje hierarchie obiektow z dokumentu FXML.
     *
     * @return fxmlloader - obiekt loadera
     */
    public static FXMLLoader getLoader() {
        return fxmlLoader;
    }

    /**
     * Zwraca glowny kontener aplikacji JavaFX. Metoda  pozwala na wczytanie przestrzeni nazw
     * i znalezienie konkretnego obiektu po jego identyfikatorze.
     *
     * @return stage - glowny kontener aplikacji
     */
    public static Stage getstage() {
        return stage;
    }

    /**
     * Uruchamia glowne okno aplikacji oraz ustawia jego parametry. Wywoluje setTable()
     * w celu ustawienia parametrow tabeli oraz wypisuje wiadomosc powitalna.
     *
     * @param stage glowny kontener okna aplikacji
     */
    @Override
    public void start(Stage stage) {
        App.stage = stage;
        stage.setMinHeight(250.0);
        stage.setMinWidth(900.0);
        fxmlLoader = new FXMLLoader(getClass().getResource("view.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            root.getStylesheets().add(getClass().getResource("/st.css").toExternalForm());
            stage.setScene(new Scene(root));
            stage.setTitle("Zarządzanie bazą danych napojów");
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Controller c = fxmlLoader.getController();
        c.setTable();
        Controller.log("Witaj w programie do zarządzania bazą napojów!");
        Controller.log("Żeby rozpocząć pracę, nawiąż połączenie z bazą.");
        Controller.log("Aby usunąć/zaktualizować wpis, najpierw go zaznacz.");
        Controller.log("Jeśli baza danych nie została jeszcze utworzona, uruchom program 'javadb'.");
        Controller.log("Miłego korzystania!");
        Controller.log(" ");
    }

}
