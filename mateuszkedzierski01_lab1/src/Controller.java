import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;


/**
 * Klasa odpowiedzialna za sterowanie mechanizmami aplikacji na podstawie interakcji uzytkownika z
 * interfejsem. Definiuje dzialanie przyciskow i rejestrowanie logu ("konsoli").
 */
public class Controller {

    //Wiersz tabeli wybrany przez użytkownika
    private static Napoj selected;
    //Komponenty FXML
    public TextArea console;
    public Text addTitle;
    public TextField nazwaFieldAdd;
    public TextField typFieldAdd;
    public TextField smakFieldAdd;
    public TextField kolorFieldAdd;
    public TextField cenaFieldAdd;
    public CheckBox plus18cbAdd;
    public Button addButton;
    public Button clearButton;
    public Button cancelAddButton;
    public Text updateTitle;
    public TextField nazwaFieldUpdate;
    public TextField typFieldUpdate;
    public TextField smakFieldUpdate;
    public TextField kolorFieldUpdate;
    public TextField cenaFieldUpdate;
    public CheckBox plus18cbUpdate;
    public Button updateUpdateButton;
    public Button cancelUpdateButton;
    public Button connectButton;
    public Button updateButton;
    public Button removeButton;
    public Button disconnectButton;
    public Button addAddButton;
    public Button refreshButton;
    public Button importButton;
    //Widok TableView
    public TableColumn<Integer, Napoj> idCol;
    public TableColumn<String, Napoj> nazwaCol;
    public TableColumn<String, Napoj> typCol;
    public TableColumn<String, Napoj> smakCol;
    public TableColumn<String, Napoj> kolorCol;
    public TableColumn<Integer, Napoj> cenaCol;
    public TableColumn<Boolean, Napoj> czy18Col;
    public TableView<Napoj> table;

    /**
     * Wypisuje informacje w polu konsoli
     *
     * @param line wypisywany ciag znakow
     */
    public static void log(String line) {
        TextArea cons = (TextArea) App.getstage().getScene().lookup("#console");
        cons.appendText(line + "\n");
        //cons.setScrollTop(Double.MAX_VALUE);
    }

    /**
     * Aktualizuje zawartosc tabeli poprzez ponowne wczytanie z bazy i sortuje zawartosc tabeli.
     */
    @FXML
    private static void updateTable() {
        Controller c = App.getLoader().getController();
        TableView<Napoj> tab = c.getTable();

        //table.getItems().clear();
        ObservableList<Napoj> list = null;
        try {
            list = DBManager.getData();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assert list != null;
        list.sort((o1, o2) -> o1.getId() > o2.getId() ? 1 : 0);
        SortedList<Napoj> l2 = new SortedList<>(list);
        l2.comparatorProperty().bind(tab.comparatorProperty());
        tab.setItems(l2);
    }

    /**
     * Sprawdza poprawnosc danych podanych jako argumentu
     *
     * @param nazwa   nazwa napoju
     * @param typ     typ napoju
     * @param smak    smak napoju
     * @param kolor   kolor napoju
     * @param cenastr cena napoju w postaci ciagu znakow
     * @return tablica zmiennych Boole'owskich, true oznacza poprawne, false - niepoprawne dane.
     */
    public static Boolean[] valNapoj(String nazwa, String typ, String smak, String kolor, String cenastr) {
        Boolean[] arr = {true, true, true, true, true, true};
        if (nazwa.isEmpty() || nazwa.length() > 20) arr[1] = false;
        if (typ.isEmpty() || typ.length() > 15) arr[2] = false;
        if (smak.isEmpty() || smak.length() > 10) arr[3] = false;
        if (kolor.isEmpty() || kolor.length() > 10) arr[4] = false;
        int c;
        try {
            c = Integer.parseInt(cenastr);
            if (c == 0) arr[5] = false;
        } catch (NumberFormatException e) {
            arr[5] = false;
        }
        return arr;
    }

    /**
     * Nawiazuje polaczenie z baza, uaktywnia przyciski dzialajace tylko podczas polaczenia
     * i dezaktywuje przycisk laczenia. Ustawia tekst pustej tabeli i aktualizuje tabele.
     */
    @FXML
    public void connect() {
        try {
            if (!DBManager.isConnected()) {
                DBManager.connect();
                addButton.setDisable(false);
                disconnectButton.setDisable(false);
                connectButton.setDisable(true);
                refreshButton.setDisable(false);
                importButton.setDisable(false);
                log("Połączono z bazą!");
                table.setPlaceholder(new Label("Baza jest pusta!"));
                updateTable();
            } else log("Połączenie jest już ustanowione!");
        } catch (SQLException e) {
            log(e.getMessage());
        }
    }

    /**
     * Przeladowuje zawartosc tabeli i wypisuje komunikat.
     */
    @FXML
    public void refresh() {
        updateTable();
        log("Dane w tabeli zaktualizowane");
    }

    /**
     * Konczy polaczenie z baza danych, dezaktywuje przyciski dzialajace tylko w trybie polaczenia,
     * oproznia tabele, wypisuje stosowny komunikat.
     */
    @FXML
    public void disconnect() {
        try {
            if (DBManager.isConnected()) {
                DBManager.disconnect();
                connectButton.setDisable(false);
                addButton.setDisable(true);
                disconnectButton.setDisable(true);
                refreshButton.setDisable(true);
                importButton.setDisable(true);
                table.setItems(null);
                log("Rozłączono z bazą!");
                table.setPlaceholder(new Label("Brak połączenia z bazą!"));
            } else log("Brak połączenia z bazą!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    //TU WSZYSTKO OD DODAWANIA

    /**
     * Usuwa rekord z bazy danych pobierajac id zaznaczonego wiersza tabeli.
     */
    public void remove() {
        //System.out.println("Usuwam wpis o ID: " + selected.getId());
        try {
            int rows = DBManager.delete(selected);
            if (rows == 1) log("Usunięto wpis: " + selected.getNazwa());
            else log("Tego rekordu nie znaleziono w bazie!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        updateTable();
        //table.getItems().remove(table.getSelectionModel().getSelectedIndex());
    }

    /**
     * Wyswietla okno wprowadzania danych do dodania
     *
     * @throws IOException brak pliku CSS lub FXML
     */
    public void ShowAdd() throws IOException {
        Stage addStage = new Stage();
        addStage.initModality(Modality.WINDOW_MODAL);
        addStage.initOwner(console.getScene().getWindow());
        addStage.setMinHeight(380);
        addStage.setMinWidth(500);
        addStage.setMaxHeight(380);
        addStage.setMaxWidth(500);
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getClassLoader().getResource("view_add.fxml")));
        root.getStylesheets().add(getClass().getResource("/st.css").toExternalForm());
        addStage.setScene(new Scene(root));
        addStage.setTitle("Dodawanie rekordu");
        addStage.show();
    }

    /**
     * Zamyka okno dodawania wpisu
     */
    public void closeAdd() {
        Stage stage = (Stage) cancelAddButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Czysci pola wprowadzania danych do dodania i resetuje czerwone obramowania.
     */
    public void clearAdd() {
        TextField[] addFields = {nazwaFieldAdd, typFieldAdd, smakFieldAdd, kolorFieldAdd, cenaFieldAdd};
        for (TextField f : addFields) {
            f.setText("");
            f.setStyle("-fx-border-style: none");
        }
        plus18cbAdd.setSelected(false);
    }

    //TU WSZYSTKO OD UPDATE

    /**
     * Weryfikuje zawartosc formularza dodawania, na jego podstawie tworzy
     * obiekt klasy Napoj i dodaje go do bazy.
     */
    public void add() {
        TextField[] addFields = {nazwaFieldAdd, typFieldAdd, smakFieldAdd, kolorFieldAdd, cenaFieldAdd};
        int id = DBManager.getFirstFreeID();
        String nazwa = nazwaFieldAdd.getText();
        String typ = typFieldAdd.getText();
        String smak = smakFieldAdd.getText();
        String kolor = kolorFieldAdd.getText();
        String cena = cenaFieldAdd.getText();
        boolean czy18pl = plus18cbAdd.isSelected();

        Boolean[] validate = valNapoj(nazwa, typ, smak, kolor, cena);
        List<Boolean> valList = Arrays.asList(validate);
        if (valList.contains(false)) {
            while (valList.contains(false)) {
                int i = valList.indexOf(false);
                addFields[i - 1].setStyle("-fx-border-color: red; -fx-border-width: 2px");
                valList.set(i, true);
            }
            return;
        }
        try {
            Napoj n = new Napoj(id, nazwa, typ, smak, kolor, Integer.parseInt(cena), czy18pl);
            DBManager.insertData(n);
            closeAdd();
            log("Dodano '" + nazwa + "' do bazy.");
            updateTable();
        } catch (SQLException | NumberFormatException e) {
            e.printStackTrace();
        }
    }

    /**
     * Wyswietla okno aktualizowania rekordu i wczytuje do niego dane edytowanego wiersza.
     *
     * @throws IOException brak pliku CSS lub FXML
     */
    public void ShowUpdate() throws IOException {
        Stage updateStage = new Stage();
        updateStage.setMinHeight(150.0);
        updateStage.setMinWidth(100.0);
        updateStage.initModality(Modality.WINDOW_MODAL);
        updateStage.initOwner(console.getScene().getWindow());
        FXMLLoader fxmlLoader;
        fxmlLoader = new FXMLLoader(getClass().getResource("view_update.fxml"));
        Parent root = fxmlLoader.load();
        root.getStylesheets().add(getClass().getResource("/st.css").toExternalForm());
        updateStage.setScene(new Scene(root));
        updateStage.setTitle("Aktualizacja rekordu");
        updateStage.show();
        TextField[] fields = {
                (TextField) fxmlLoader.getNamespace().get("nazwaFieldUpdate"),
                (TextField) fxmlLoader.getNamespace().get("typFieldUpdate"),
                (TextField) fxmlLoader.getNamespace().get("smakFieldUpdate"),
                (TextField) fxmlLoader.getNamespace().get("kolorFieldUpdate"),
                (TextField) fxmlLoader.getNamespace().get("cenaFieldUpdate")
        };
        CheckBox cb = (CheckBox) fxmlLoader.getNamespace().get("plus18cbUpdate");
        Napoj n = table.getSelectionModel().getSelectedItem();
        fields[0].setText(n.getNazwa());
        fields[1].setText(n.getTyp());
        fields[2].setText(n.getSmak());
        fields[3].setText(n.getKolor());
        fields[4].setText(Integer.toString(n.getCena()));
        cb.setSelected(n.is18plus());
    }

    /**
     * Zamyka okno aktualizowania rekordu
     */
    public void closeUpdate() {
        Stage stage = (Stage) cancelUpdateButton.getScene().getWindow();
        stage.close();
    }

    //TU DZIALANIE NIEKTORYCH ELEMENTOW

    /**
     * Weryfikuje zawartosc formularza aktualizacji rekordu i aktualizuje rekord
     * wczesniej zaznaczony w tabeli.
     */
    @FXML
    public void update() {
        TextField[] updateFields = {nazwaFieldUpdate, typFieldUpdate, smakFieldUpdate, kolorFieldUpdate, cenaFieldUpdate};
        CheckBox cb = plus18cbUpdate;
        int id = selected.getId();
        String nazwa = updateFields[0].getText();
        String typ = updateFields[1].getText();
        String smak = updateFields[2].getText();
        String kolor = updateFields[3].getText();
        String cena = updateFields[4].getText();
        boolean czy18pl = cb.isSelected();

        Boolean[] validate = valNapoj(nazwa, typ, smak, kolor, cena);
        List<Boolean> valList = Arrays.asList(validate);
        if (valList.contains(false)) {
            while (valList.contains(false)) {
                int i = valList.indexOf(false);
                updateFields[i - 1].setStyle("-fx-border-color: red; -fx-border-width: 2px");
                valList.set(i, true);
            }
            return;
        }
        try {
            Napoj n = new Napoj(id, nazwa, typ, smak, kolor, Integer.parseInt(cena), czy18pl);
            DBManager.updateData(n);
            closeUpdate();
            log("Zaktualizowano '" + nazwa + "' w bazie");
            updateTable();
        } catch (SQLException | NumberFormatException e) {
            e.printStackTrace();
        }
    }

    /**
     * Resetuje styl pol tekstowych po nacisnieciu na nie
     *
     * @param mouseEvent zdarzenie polegajace na kliknieciu w obrebie pola tekstowego
     */
    public void resetStyle(MouseEvent mouseEvent) {
        TextField src = (TextField) mouseEvent.getSource();
        src.setStyle("-fx-border-style: none");
    }

    /**
     * Przygotowuje tabele, czyli ustawia wlasciwosci komorek, rozmiary kolumn, ustala zachowanie
     * po zaznaczeniu ktoregos z wierszy.
     */
    @FXML
    public void setTable() {
        table.setId("table");
        //ustawienie zawartości kolumn
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        nazwaCol.setCellValueFactory(new PropertyValueFactory<>("nazwa"));
        typCol.setCellValueFactory(new PropertyValueFactory<>("typ"));
        smakCol.setCellValueFactory(new PropertyValueFactory<>("smak"));
        kolorCol.setCellValueFactory(new PropertyValueFactory<>("kolor"));
        cenaCol.setCellValueFactory(new PropertyValueFactory<>("cena"));
        czy18Col.setCellValueFactory(new PropertyValueFactory<>("czy18string"));

        //ustawienie szerokości kolumn względem rozmiaru tabeli
        idCol.minWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.05));
        idCol.maxWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.05));
        nazwaCol.minWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.2));
        nazwaCol.maxWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.2));
        typCol.minWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.2));
        typCol.maxWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.2));
        smakCol.minWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.2));
        smakCol.maxWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.2));
        kolorCol.minWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.2));
        kolorCol.maxWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.2));
        cenaCol.minWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.1));
        cenaCol.maxWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.1));
        czy18Col.minWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.05));
        czy18Col.maxWidthProperty().bind(table.widthProperty().subtract(5).multiply(0.05));

        table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (table.getSelectionModel().getSelectedCells().size() == 0) {
                Button removeButton = (Button) App.getstage().getScene().lookup("#removeButton");
                Button updateButton = (Button) App.getstage().getScene().lookup("#updateButton");
                removeButton.setDisable(true);
                updateButton.setDisable(true);
            } else {
                selected = table.getSelectionModel().getSelectedItem();
                Button removeButton = (Button) App.getstage().getScene().lookup("#removeButton");
                Button updateButton = (Button) App.getstage().getScene().lookup("#updateButton");
                removeButton.setDisable(false);
                updateButton.setDisable(false);
            }

        });


    }

    /**
     * Zwraca obiekt tabeli (TableView)
     *
     * @return obiekt tabeli (TableView)
     */
    TableView<Napoj> getTable() {
        return table;
    }

    /**
     * Wczytuje dane z pliku CSV, sprawdza poprawnosc i dodaje do bazy, a nastepnie aktualizuje tabele.
     */
    public void importCSV() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Wybierz plik CSV");
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Dane rozdzielane przecinkami", "*.csv"));
        fc.setInitialDirectory(new File(System.getProperty("user.dir")));
        File csv = fc.showOpenDialog(App.getstage());
        if (csv == null) return;
        Scanner sc = null;
        try {
            sc = new Scanner(csv);
        } catch (FileNotFoundException e) {
            System.err.println("Brak pliku");
        }
        if (sc != null) {
            String line;
            sc.nextLine();
            while (sc.hasNextLine()) {
                line = sc.nextLine().replace("'", "");
                //System.out.println(line);
                String[] args = line.split(",");

                Boolean[] validate = valNapoj(args[0], args[1], args[2], args[3], args[4]);
                List<Boolean> valList = Arrays.asList(validate);
                if (valList.contains(false)) {
                    log("Rekord '" + args[0] + "' zawiera niepoprawne dane (" + args[valList.indexOf(false)] + ")");
                    continue;
                }
                try {
                    Napoj n = new Napoj(DBManager.getFirstFreeID(), args[0], args[1], args[2], args[3], Integer.parseInt(args[4]), Boolean.parseBoolean(args[5]));
                    DBManager.insertData(n);
                    log("Dodano '" + args[0] + "' do bazy.");
                } catch (NumberFormatException e) {
                    log(e.getMessage());
                } catch (SQLException e) {
                    e.printStackTrace();
                }


            }
            updateTable();
        }


    }
}
