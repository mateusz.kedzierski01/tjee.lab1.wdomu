public class Napoj {
    private int id;
    private String nazwa;
    private String typ;
    private String smak;
    private String kolor;
    private int cena;
    private boolean czy18plus;
    private String czy18string;

    public Napoj(int id, String nazwa, String typ, String smak, String kolor, int cena, boolean czy18plus) {
        this.id = id;
        this.nazwa = nazwa;
        this.typ = typ;
        this.smak = smak;
        this.kolor = kolor;
        this.cena = cena;
        this.czy18plus = czy18plus;
        this.czy18string = czy18plus ? "TAK" : "NIE";
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public int getId() {
        return id;
    }

    public String getSmak() {
        return smak;
    }

    public String getKolor() {
        return kolor;
    }

    public int getCena() {
        return cena;
    }

    public boolean is18plus() {
        return czy18plus;
    }

    public String getCzy18string() {
        return czy18string;
    }

}
