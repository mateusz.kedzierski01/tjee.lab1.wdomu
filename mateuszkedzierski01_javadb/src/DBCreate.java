import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;

public class DBCreate {
    public static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String PROTOCOL = "jdbc:derby:";
    public static final String DB_PATH = "mateuszkedzierski01_db";
    public static final String JDBC_URL = PROTOCOL + DB_PATH;
    public static final String JDBC_CREATE_URL = PROTOCOL + DB_PATH + ";create=true";
    private static Connection conn;

    public static void main(String[] args) {

        // Sprawdzanie czy baza juz istnieje
        System.out.println("Szukam poprzedniej bazy. . .");
        try {
            //connect();
            //if (conn != null) {
            File f = new File("mateuszkedzierski01_db");
            if (f.exists()) {
                System.out.println("Baza już istnieje! Nadpisać? (T/N)");
                Scanner s = new Scanner(System.in);
                String r = s.nextLine().toLowerCase();
                if (r.equals("t")) {
                    System.out.println("Usuawam poprzednią bazę");

                    //System.out.println(f.getCanonicalFile());
                    deleteDatabase(Paths.get(f.getAbsolutePath()));

                } else {
                    System.out.println("Nie utworzono bazy.");
                    return;
                }
            }
        } catch (IOException e) {
            System.out.println("Nie znaleziono");
        }


        // tworzenie nowej bazy
        try {
            System.out.println("Tworzę nową bazę danych. . .");
            setupDB();
            System.out.println("Utworzono bazę!");
        } catch (SQLException e) {
            System.err.println("Nie udało się utworzyć bazy: " + e.getMessage());
            return;
        }

        // tworzenie tabeli
        try {
            System.out.println("Tworzę tabelę");
            createTable();
            System.out.println("Utworzono");
        } catch (SQLException e) {
            System.err.println("Nie udało się utworzyć tabeli: " + e.getMessage());
            return;
        }

        // wprowadzanie danych
        try {
            System.out.println("Wprowadzam dane do bazy");
            insertData();
            System.out.println("Wprowadzono!");
        } catch (SQLException e) {
            System.err.println("Nie udało się wprowadzić danych: " + e.getMessage());
            return;
        }

        try {
            disconnect();
        } catch (SQLException e) {
            System.err.println("Nie udało się zakończyć połączenia z bazą!");

        }
    }

    private static void createTable() throws SQLException {
        Statement s = conn.createStatement();
        String polecenie = "create table MATEUSZKEDZIERSKI01_NAPOJE_TB" +
                "(" +
                "    ID_NAPOJU                  INTEGER not null" +
                "        constraint \"mateusz.kedzierski01_napoje_tb_pk\"" +
                "            primary key," +
                "    NAZWA                      VARCHAR(20)," +
                "    TYP                        VARCHAR(15)," +
                "    SMAK                       VARCHAR(10)," +
                "    KOLOR                      VARCHAR(10)," +
                "    CENA_PLN                   INTEGER," +
                "    CZY_TYLKO_DLA_PELNOLETNICH BOOLEAN)";

        s.execute(polecenie);
        s.close();
    }

    public static void connect() throws SQLException {
        conn = DriverManager.getConnection(JDBC_URL);
    }

    private static void setupDB() throws SQLException {
        conn = DriverManager.getConnection(JDBC_CREATE_URL);
    }

    public static void disconnect() throws SQLException {
        conn.close();
        conn = null;
    }

    public static ObservableList<Napoj> getData() throws SQLException {
        Statement stat = conn.createStatement();
        String query = "SELECT * FROM MATEUSZKEDZIERSKI01_NAPOJE_TB";
        ResultSet rs = stat.executeQuery(query);

        List<Napoj> list = new ArrayList<>();
        while (rs.next()) {
            Object[] args = {rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getBoolean(7)};
            Boolean[] val = valNapoj((String) args[1], (String) args[2], (String) args[3], (String) args[4], args[5].toString());
            List<Boolean> valList = Arrays.asList(val);
            if (valList.contains(false)) {
                System.out.println("Niepoprawne dane w bazie! ID: " + args[0] + " nazwa: " + args[1]);
                continue;
            }
            Napoj n = new Napoj((int) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4], (int) args[5], (boolean) args[6]);
            list.add(n);
        }
        stat.close();
        return FXCollections.observableList(list);
    }

    public static void insertData() throws SQLException {
        Statement stat = conn.createStatement();

        String polecenie = "INSERT INTO MATEUSZKEDZIERSKI01_NAPOJE_TB("
                + "ID_NAPOJU, NAZWA, TYP, SMAK, KOLOR, CENA_PLN, CZY_TYLKO_DLA_PELNOLETNICH) VALUES "
                + "(0, 'CocaCola', 'gazowany', 'słodki', 'czarny', 4, false), "
                + "(1, 'Sprite', 'gazowany', 'słodki', 'bezbarwny', 4, false), "
                + "(2, 'Tyskie', 'piwo', 'piwny', 'złocisty', 3, true), "
                + "(3, 'Muszynianka', 'woda mineralna', 'magnezowy', 'bezbarwny', 2, false), "
                + "(4, 'Tymbark', 'owocowy', 'słodki', 'czerwony', 4, false), "
                + "(5, 'Żołądkowa DeLuxe', 'wódka', 'gorzki', 'bezbarwny', 30, true), "
                + "(6, 'Lipton', 'ice tea', 'słodki', 'brązowy', 3, false), "
                + "(7, 'Jack Daniels', 'whiskey', 'dębowy', 'brązowy', 75, true), "
                + "(8, 'MountainDew', 'gazowany', 'słodki', 'zielony', 4, false), "
                + "(9, 'Cisowianka', 'woda mineralna', 'neutralny', 'bezbarwny', 2, false), "
                + "(10, 'Harnaś', 'piwo', 'piwny', 'złocisty', 2, true)";
        stat.executeUpdate(polecenie);
        stat.close();
    }

    public static void deleteDatabase(Path path) throws IOException {
        Files.walk(path)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }


    /**
     * Sprawdza poprawnosc danych podanych jako argumentu
     *
     * @param nazwa   nazwa napoju
     * @param typ     typ napoju
     * @param smak    smak napoju
     * @param kolor   kolor napoju
     * @param cenastr cena napoju w postaci ciagu znakow
     * @return tablica zmiennych Boole'owskich, true oznacza poprawne, false - niepoprawne dane.
     */
    public static Boolean[] valNapoj(String nazwa, String typ, String smak, String kolor, String cenastr) {
        Boolean[] arr = {true, true, true, true, true, true};
        if (nazwa.isEmpty() || nazwa.length() > 20) arr[1] = false;
        if (typ.isEmpty() || typ.length() > 15) arr[2] = false;
        if (smak.isEmpty() || smak.length() > 10) arr[3] = false;
        if (kolor.isEmpty() || kolor.length() > 10) arr[4] = false;
        int c;
        try {
            c = Integer.parseInt(cenastr);
            if (c == 0) arr[5] = false;
        } catch (NumberFormatException e) {
            arr[5] = false;
        }
        return arr;
    }

}

