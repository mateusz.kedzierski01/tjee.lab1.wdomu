Repozytorium zawiera dwa projekty środowiska intelliJ oraz folder z dwoma plikami wykonywalnymi. Aby samodzielnie skompilować
programy, należy zklonować repo, a następnie otworzyć folder wybranego projektu przez IDE. 

mateuszkedzierski01_lab1 - główny program do zarządzania bazą
mateuszkedzierski01_javadb - program tworzący bazę danych i wprowadzający do niej podstawowe dane.

W programach użyto nazwy (loginu) z GitLab ale z pominięciem kropki, w celu uniknięcia problemów związanych
z poleceniami SQL i innymi aspektami programowania w Java.

